Gem::Specification.new do |s|
  s.name        = 'rubygem'
  s.version     = ENV["VERSION"]
  s.summary     = "Hello!"
  s.description = "A simple hello world gem"
  s.authors     = ["Example Author"]
  s.email       = 'example@example.com'
  s.files       = ["lib/hello.rb"]
  s.homepage    =
    'https://rubygems.org/gems/hola'
  s.license       = 'MIT'
end