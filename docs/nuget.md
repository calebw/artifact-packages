# NuGet Packages & the GitLab Package Registry

## The Important Bits

When publishing an example NuGet package to the GitLab Package Registry, we really only care about to points (for this example project, at least):

- Version (kinda)
- Authentication

### Version

Unlike other packages in the GitLab Package Registry, attempting to publish a package with the same name or version of an existing package in the registry will **not** present any errors.

Instead, the package is simply overwritten with the latest publish. We still automatically update the version in the CI job for this package, simply for the sake of being uniform with how we're publishing other packages.

The version for the NuGet package is defined in the `nuget/nuget.csproj` file, in the `<VERSION>` field. For this project, it looks like so:

```xml
...
    <Version>$(VERSION)</Version>
...
```

Here we set the version to the environment variable of the same name, `VERSION`. This value is created randomly in the CI job, covered in more detail below.

### Authentication

When publishing NuGet packages, we don't need to worry about defining any configuration files that specify where we're publishing the package and how we're authenticating.

Instead, this is all handled in directly the job via the `dotnet nuget` utility. We'll touch on this more below, just know that we make use of the `CI_JOB_TOKEN` variable to authenticate, as we do with most jobs in this repository.

----

## CI Job Overview

Here's the CI job for publishing the NuGet package:

```yaml
nuget:
  image: mcr.microsoft.com/dotnet/core/sdk:3.1
  script:
    - cd nuget
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - dotnet pack -c Release
    - dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
    - dotnet nuget push "bin/Release/*.nupkg" --source gitlab
```

After moving into the `nuget/` directory, we export the `VERSION` variable for the package:

```sh
export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
# This would equate to something like VERSION=1.23.45
```

This is simply generating a random version value and assigning it to the `VERSION` environment variable, which is latter referenced by the `nuget.csproj` file. This grabs a random number between 0-9, and a random number between 0-99 for the last two decimals. Since the `nuget.csproj` file is configured to grab this value from the system environment variable directly, we do not have to worry about setting this value directly in the file itself.

We then build the package via the `dotnet` utility. This builds the `.nupkg`, `nuget.dll` and other associated files within the `bin/Relase` directory.

```sh
dotnet pack -c Release
```

After the package is built, we set the source that defines where the package will be published to, and the token that will be used to authenticate. This makes use of [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) to automtically fetch the appropriate address for your package, and the `CI_JOB_TOKEN` to authenticate.

```sh
dotnet nuget add source "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/nuget/index.json" --name gitlab --username gitlab-ci-token --password $CI_JOB_TOKEN --store-password-in-clear-text
```

With the package built, our source and authentication configured, all that's left is to push the package up:

```sh
dotnet nuget push "bin/Release/*.nupkg" --source gitlab
```
