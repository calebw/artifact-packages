# PyPI Packages & the GitLab Package Registry

## The Important Bits

When publishing a PyPI package to the GitLab Package Registry, there are just a couple important tidbits:

- Name & Version
- Authentication

### Name & Version

The `name` and `version` fields within the `setup.py` file in the `pypi` directory of this repository are responsible for defining the name and version for the PyPI package.

As with other packages in the GitLab registry, you cannot publish a package with the same name and version of an already existing package in the registry. Doing so will result in a `400 bad request` response.

For this example repository, these values look like so:

```py
VERSION = os.getenv('VERSION')

setuptools.setup(
    name="pypi",
    version=VERSION,
...
```

The name is statically set to "pypi" and does not change. The version is mapped to the `VERSION` environment variable (fetched by the first line in the snippet above) which is set to a random value within the CI job. This is touched on more in the CI Job Overview below.


### Authentication

We make use of the [twine utility](https://pypi.org/project/twine/) to handle the publishing of our package. There isn't a lot to this - we pass the authentication token and the location we'd like to publish the package at to twine and it handles the rest for us. This is done within the CI job and will be covered more below.

----

## CI Job Overview

Here's the CI job for publishing the PyPI package:

```yaml
pypi:
  image: python:3.7.10-alpine3.12
  before_script:
    - apk update && apk add python3-dev gcc libc-dev libffi-dev
    - pip install --upgrade pip
  script:
    - cd pypi/
    - export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
    - pip install twine
    - python setup.py sdist bdist_wheel
    - TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
```

After moving into the `pypi/` directory, we export the `VERSION` variable for the package:

```sh
export VERSION=$(( $RANDOM % 9 )).$(( $RANDOM % 99 )).$(( $RANDOM % 99 ))
# This would equate to something like VERSION=1.23.45
```

This is simply generating a random version value and assigning it to the `VERSION` environment variable, which is latter called by the `setup.py` file. This grabs a random number between 0-9, and a random number between 0-99 for the last two decimals. Since the `setup.py` file is configured to grab this value from the system environment variable directly, we do not have to worry about setting this value directly in the file itself.

Next, we run `pip install twine` to install the twine utility used for publishing our package.

We then build the package itself:

```sh
python setup.py sdist bdist_wheel
```

Before finally publishing the package (built within the `dist/` directory using the above command) using twine:

```sh
TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
```

Here, we tell twine to authenticate with the `CI_JOB_TOKEN` and where to publish the package within the `dist/` directory, using [predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
